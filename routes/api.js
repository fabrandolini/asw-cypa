module.exports = {

    create: function (app) {

        var self = this;

		self.app = app;
		self.db = app.db;
		self.result = app.result;
		self.uuid = app.uuid;
		
		self.getRoomFromCode = function (httpRequest, httpResult) {
			var jsonData = {
				index: 'appdb',
				type: 'room',
				id: httpRequest.body.qrCode
			};

			self.db.get(jsonData, function(error, getResult) {
				if (error) {
					httpResult.json(self.result.error(self.result.ERROR_INVALID_PARAMS));
				} else {

					var api_result = {
						qrCode: getResult._id,
						name: getResult._source.name,
						owner: getResult._source.owner
					}

					httpResult.json(self.result.ok(api_result));
				}
			});
		};
		
		self.bindQRCode =  function (httpRequest, httpResult) {

			//first of all, get old appCode
			getProfOldAppCodeQuery = {

				index: 'appdb',
				type: 'room',
				id: httpRequest.body.qrCode
			};

			self.db.get(getProfOldAppCodeQuery, function(error, getResult) {
				
				if(error){
					httpResult.json(self.result.error(self.result.ERROR_INVALID_PARAMS));
				}else{

					var oldAppCode = getResult._source.code;

					if(oldAppCode != null){//not first binding for this prof

						//then find registration with the old appCode
						var findOldRegistrationQuery = {
							index: 'appdb',
							type: 'room',
							body: {
								query: {
									match: {
										'guests.guestCode': oldAppCode
									}
								}
							}
						};



						self.db.search(findOldRegistrationQuery, function(error, searchResult) {


							if(error){
								httpResult.json(self.result.error(self.result.ERROR_DB_SEARCH));
							} else if(searchResult.hits.hits.length == 0){ //generate and update AppCode immediately

								//then generate and update the AppCode
								var newCode = "" + self.uuid.v4();

								var jsonData = {
									index: 'appdb',
									type: 'room',
									id: httpRequest.body.qrCode,
									body: {
										doc: {
											code: newCode
										}
									}
								};

								self.db.update(jsonData, function(error, updateResult2){

									if (error) {
										httpResult.json(self.result.error(self.result.ERROR_APP_CODE_UPDATE));
									} else {

										var api_result = {
											qrCode: updateResult2._id,
											appCode: newCode
										}

										httpResult.json(self.result.ok(api_result));
									}
								});

							}else{
								//otherwise delete old registration data

								//deleting the specified guest from the array

								var i;
								var array = searchResult.hits.hits[0]._source.guests;
								for(i=0;i<array.length;i++){
									if(array[i].guestCode == oldAppCode){
										array.splice(i,1);
									}
								}



								var deleteOldInfoQuery = {
									index: 'appdb',
									type: 'room',
									id: searchResult.hits.hits[0]._id,
									body: {
										doc: {
											guests : searchResult.hits.hits[0]._source.guests
										}
									}
								}


									searchResult.hits.hits[0]._source;

								self.db.update(deleteOldInfoQuery, function(error, updateResult) {

									if (error) {
										httpResult.json(self.result.error(self.result.ERROR_DELETE_OLD_BINDING));
									} else {

										//then generate and update the AppCode
										var newCode = "" + self.uuid.v4();

										var jsonData = {
											index: 'appdb',
											type: 'room',
											id: httpRequest.body.qrCode,
											body: {
												doc: {
													code: newCode
												}
											}
										};

										self.db.update(jsonData, function(error, updateResult2){

											if (error) {
												httpResult.json(self.result.error(self.result.ERROR_APP_CODE_UPDATE));
											} else {

												var api_result = {
													qrCode: updateResult2._id,
													appCode: newCode
												}

												httpResult.json(self.result.ok(api_result));
											}
										});

									}
								});
							}
						});

					}else{ //first binding for this prof;

						//generate a new AppCode
						var newCode = "" + self.uuid.v4();

						var jsonData = {
							index: 'appdb',
							type: 'room',
							id: httpRequest.body.qrCode,
							body: {
								doc: {
									code: newCode
								}
							}
						};

						self.db.update(jsonData, function(error, updateResult2){

							if (error) {
								httpResult.json(self.result.error(self.result.ERROR_APP_CODE_UPDATE));
							} else {

								var api_result = {
									qrCode: updateResult2._id,
									appCode: newCode
								}

								httpResult.json(self.result.ok(api_result));
							}
						});
					}

				}
			});
		};
		
		self.setCurrentRoom = function (httpRequest, httpResult) {

		
			//check if other binding are available
			var verifyOtherBindingsQuery = {
				index: 'appdb',
				type: 'room',
				body: {
					query: {
						match: {
							'guests.guestCode': httpRequest.body.appCode
						}
					}
				}
			};

			self.db.search(verifyOtherBindingsQuery, function(error, firstSearchResult) {
				
				if (error) {
					httpResult.json(self.result.error(self.result.ERROR_DB_SEARCH));
				} else if(firstSearchResult.hits.hits.length > 0){
					//console.log(firstSearchResult);
					//console.log(firstSearchResult.hits.hits.length);
					//httpResult.json(self.result.error(self.result.ERROR_PROF_POSITION_ALREADY_SET));
					//delete old position

					var i;
					var array = firstSearchResult.hits.hits[0]._source.guests;
					for(i=0;i<array.length;i++){
						if(array[i].guestCode == httpRequest.body.appCode){
							array.splice(i,1);
						}
					}

					var deleteProfLocationData = {
						index: 'appdb',
						type: 'room',
						id: firstSearchResult.hits.hits[0]._id,
						body: {
							doc: {
								guests : firstSearchResult.hits.hits[0]._source.guests
							}
						}
					}

					self.db.update(deleteProfLocationData, function(error, updateResult) {
						//console.log(updateResult);

						if (error) {
							httpResult.json(self.result.error(self.result.ERROR_DB_UPDATE));
						} else {
							//get professor's info
							var getProfInfoQuery = {
								index: 'appdb',
								type: 'room',
								body: {
									query: {
										match: {
											code: httpRequest.body.appCode
										}
									}
								}
							};

							self.db.search(getProfInfoQuery, function(error, searchResult) {
								if (error) {
									httpResult.json(self.result.error(self.result.ERROR_PROFESSOR_INFO_UNAVAILABLE));
								} else {

									var getRoomInfoQuery = {
										index: 'appdb',
										type: 'room',
										id: httpRequest.body.qrCode
									};

									//get the information of the room that has to be set, to change the guests list
									self.db.get(getRoomInfoQuery, function(error, roomInfoResult) {
										if (error) {
											httpResult.json(self.result.error(self.result.ERROR_CAN_NOT_FIND_ROOM));
										} else {

											var profData = searchResult.hits.hits[0];

											var currentTimestamp = new Date().getTime();
											//httpResult.json(roomInfoResult);
											var array = roomInfoResult._source.guests;

											//console.log(array);

											var object = {
												guestCode: httpRequest.body.appCode,
												timestamp: currentTimestamp,
												guestName: profData._source.owner
											}
											array.push(object);

											//console.log(array);

											var setProfLocationData = {
												index: 'appdb',
												type: 'room',
												id: httpRequest.body.qrCode,
												body: {
													doc: {
														guests : array
													}
												}
											};

											self.db.update(setProfLocationData, function(error, updateResult) {

												if (error) {
													httpResult.json(self.result.error(self.result.ERROR_DB_UPDATE));
												} else {

													var result = {timestamp: currentTimestamp};

													httpResult.json(self.result.ok(result));
												}
											});

										}
									});

								}
							});
						}
					});

				}else{

					//get professor's info
					var getProfInfoQuery = {
						index: 'appdb',
						type: 'room',
						body: {
							query: {
								match: {
									code: httpRequest.body.appCode
								}
							}
						}
					};

					self.db.search(getProfInfoQuery, function(error, searchResult) {
						if (error) {
							httpResult.json(self.result.error(self.result.ERROR_PROFESSOR_INFO_UNAVAILABLE));
						} else {

							var getRoomInfoQuery = {
								index: 'appdb',
								type: 'room',
								id: httpRequest.body.qrCode
							};

							//get the information of the room that has to be set, to change the guests list
							self.db.get(getRoomInfoQuery, function(error, roomInfoResult) {
								if (error) {
									httpResult.json(self.result.error(self.result.ERROR_CAN_NOT_FIND_ROOM));
								} else {

									var profData = searchResult.hits.hits[0];

									var currentTimestamp = new Date().getTime();
									//httpResult.json(roomInfoResult);
									var array = roomInfoResult._source.guests;

									//console.log(array);

									var object = {
										guestCode: httpRequest.body.appCode,
										timestamp: currentTimestamp,
										guestName: profData._source.owner
									}
									array.push(object);

									//console.log(array);

									var setProfLocationData = {
										index: 'appdb',
										type: 'room',
										id: httpRequest.body.qrCode,
										body: {
											doc: {
												guests : array
											}
										}
									};



									self.db.update(setProfLocationData, function(error, updateResult) {

										if (error) {
											httpResult.json(self.result.error(self.result.ERROR_DB_UPDATE));
										} else {

											var result = {timestamp: currentTimestamp};

											httpResult.json(self.result.ok(result));
										}
									});

								}
							});

						}
					});
				};
			});
		};

		self.getCurrentRoom = function (httpRequest, httpResult){
			//check if other binding are available
			var findOldRegistrationQuery = {
				index: 'appdb',
				type: 'room',
				body: {
					query: {
						match: {
							'guests.guestCode': httpRequest.body.appCode
						}
					}
				}
			};

			self.db.search(findOldRegistrationQuery, function(error, searchResult) {
				console.log(searchResult);

				if(error) {
					httpResult.json(self.result.error(self.result.ERROR_DB_SEARCH));
				}else if(searchResult.hits.hits.length == 0) {
					httpResult.json(self.result.error(self.result.ERROR_PROF_IS_NOT_IN_A_ROOM));
				}else{
					var result;
					for(var i=0; i<searchResult.hits.hits[0]._source.guests.length;i++){
						if(searchResult.hits.hits[0]._source.guests[i].guestCode == httpRequest.body.appCode){
							result = {
								room: searchResult.hits.hits[0]._source.name,
								timestamp: searchResult.hits.hits[0]._source.guests[i].timestamp
							};
						}
					}

					httpResult.json(self.result.ok(result));
				}
			});
		};



		self.deleteCurrentRoom = function(httpRequest, httpResult){
			//get room info
			var getroomInfoQuery = {
				index: 'appdb',
				type: 'room',
				body: {
					query: {
						match: {
							'guests.guestCode': httpRequest.body.appCode
						}
					}
				}
			};
			
			self.db.search(getroomInfoQuery, function(error, searchResult) {

				if (error) {
					httpResult.json(self.result.error(self.result.ERROR_DB_SEARCH));
				} else if(searchResult.hits.hits.length == 0){
					httpResult.json(self.result.error(self.result.ERROR_CAN_NOT_FIND_ROOM));
				} else {

					var i;
					var array = searchResult.hits.hits[0]._source.guests;
					for(i=0;i<array.length;i++){
						if(array[i].guestCode == httpRequest.body.appCode){
							array.splice(i,1);
						}
					}

					var deleteProfLocationData = {
						index: 'appdb',
						type: 'room',
						id: searchResult.hits.hits[0]._id,
						body: {
							doc: {
								guests : searchResult.hits.hits[0]._source.guests
							}
						}
						//guests = searchResult.hits.hits[0]._source.guests
					}

					self.db.update(deleteProfLocationData, function(error, updateResult) {
						if (error) {
							httpResult.json(self.result.error(self.result.ERROR_DB_UPDATE));
						} else {
							httpResult.json(self.result.ok());
						}
					});

				}
			});
		};

		self.getAvailableProfessors = function (httpRequest, httpResult) {

			var jsonData = {
				index: 'appdb',
				type: 'room',
				body: {
					query: {
						filtered: {
						  query: {
							match_all: {}
						  },
						  filter: [
							{
							  bool: {
								must_not: {
								  missing: {
									field: 'guests.guestCode'
								  }
								}
							  }
							}
						  ]
						}
					}
				}
			};

			self.db.search(jsonData, function (error, searchResult) {
				if (error) {
					httpResult.json(self.result.error(self.result.ERROR_DB_SEARCH));
				} else {
					var nAvailable = searchResult.hits.hits.length;
					var joinResult = {availableProfessors:[]};
					//console.log(searchResult.hits);
					for(var i = 0; i<nAvailable; i++){
						//console.log(searchResult.hits.hits[i]);
						//console.log(i);
						var searchResultElement = searchResult.hits.hits[i]._source;
						if(searchResultElement.guests.length > 0) {
							for(var j=0;j<searchResultElement.guests.length;j++) {
								joinResult.availableProfessors.push({
									room: searchResultElement.name,
									guestProf: searchResultElement.guests[j].guestName,
									timestamp: searchResultElement.guests[j].timestamp
								});
							}
						}
					}

					httpResult.json(self.result.ok(joinResult));
				}
			});
		};
		

        return self;

    }

};