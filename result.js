module.exports = {

    create: function () {

        var self = this;


        /**
         *  Initialization
         */
        self.initialize = function() {
            self.ERROR_INVALID_PARAMS            	=   1000;
            self.ERROR_DB_UPDATE                 	=   1001;
            self.ERROR_PROF_INFO_UNAVAILABLE		=   1002;
			self.ERROR_DB_SEARCH	              	=   1003;
			self.ERROR_PROF_IS_NOT_IN_A_ROOM		=	1004;
			self.ERROR_CAN_NOT_FIND_ROOM			=	1005;
			self.ERROR_APP_CODE_UPDATE				= 	1006;
			self.ERROR_DELETE_OLD_BINDING			=	1007;
        };


        /**
		 * Makes an ERROR Json
		 *
		 * @param errorCode
		 * @param errorMessage
		 * @returns {{success: boolean, error: *}}
		 */
		self.error = function(errorCode) {
			return {
				"success" : false,
				"errorCode" : errorCode
			};
		};

		/**
		 * Makes an OK Json
		 *
		 * @param data
		 * @returns {{success: boolean, data: *}}
		 */
		self.ok = function(data) {
			//console.log(data);
			return {
				"success" : true,
				"data" : data
			};
		};

        return self;

    }

};