var app = angular.module('myApp', []);


app.controller('Controller', ['$scope', '$http', function($scope, $http) {

    var call = function() {
        $http
            .get('http://aswappnode-uniboapps.rhcloud.com/api/getAvailableProfessors')
            .then(successCallback, errorCallback);
    }

    var repeatCall = function() {
        setTimeout(call, 3000);
    }

    var fromMillisToDate = function(millis) {
        var date = new Date(millis);
        var hours = date.getHours();
        var minutes = "0" + date.getMinutes();
        return hours + ':' + minutes.substr(-2);
    }

    var errorCallback = function(data) {
        console.log("Error: "+data);
        repeatCall();
    }

    var successCallback = function(response) {
        var result = response.data;
        if (!result.success) {
            //showError($scope, result.error);
            console.log("Error, pleaese retry");
            return;
        }
        console.log("Data: "+result.data);
        $scope.availableProfessors = [];
        $.each(result.data.availableProfessors, function(i, item) {
            $scope.availableProfessors.push({
                "guestProf" : item.guestProf,
                "room" : item.room,
                "timestamp" : fromMillisToDate(item.timestamp)
            });
        });
        repeatCall();
    }

    call();

}]);



